export default [
    {
        date: '13 January 2018',
        team: 'Team 5 (Walton)',
        id: '1',
        startTime: '12:00:00',
        endTime: '16:00:00',
        patrollers: [
            {
                name: 'Alex Walton',
                id: '1',
                awards: [
                    'BM', 'IRBC', 'IRBD', 'SPINAL', 'FA', 'ARTC', 'GM', 'BBM', 'RWC'
                ],
                role: 'PC'
            },
            {
                name: 'Claire Walton',
                id: '2',
                awards: [
                    'BM', 'IRBC', 'IRBD', 'FA'
                ],
                role: 'IRBD'
            },
            {
                name: 'Alex Judd',
                id: '3',
                awards: [
                    'FA'
                ],
                role: 'VC'
            },
            {
                name: 'Jai Sullivan',
                id: '4',
                awards: [
                    'BM', 'IRBC', 'SPINAL'
                ],
                role: 'IRBC'
            }
        ]
    },
    {
        date: '14 January 2018',
        team: 'Team 5 (Walton)',
        id: '2',
        startTime: '1200',
        endTime: '1600',
        patrollers: [
            {
                name: 'Alex Walton',
                id: '1',
                awards: [
                    'BM', 'IRBC', 'IRBD', 'SPINAL', 'FA', 'ARTC', 'GM', 'BBM', 'RWC'
                ],
                role: 'PC'
            },
            {
                name: 'Claire Walton',
                id: '2',
                awards: [
                    'BM', 'IRBC', 'IRBD', 'FA'
                ],
                role: 'IRBD'
            },
            {
                name: 'Alex Judd',
                id: '3',
                awards: [
                    'FA'
                ],
                role: 'VC'
            },
            {
                name: 'Jai Sullivan',
                id: '4',
                awards: [
                    'BM', 'IRBC', 'SPINAL'
                ],
                role: 'IRBC'
            }
        ]
    }
];