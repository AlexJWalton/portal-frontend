import { createStore, applyMiddleware } from 'redux';
import reducer from './reducers/main';

import {
    middleware as flashMiddleware,
} from 'redux-flash'

const flashOptions = {
    timeout: 4000
}

const store = createStore(
    reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(flashMiddleware(flashOptions))
);

export default store;
