import React, {Component} from 'react';
import PropTypes from 'prop-types';

import store from './../store';

export default (BaseComponent) => {

    let authenticationCheck = () => {
        console.log(window.location.pathname);
        let state = store.getState().session;
        return (state.loggedIn == true);
    }

    // class CheckLoggedIn extends Component
    // {
    //     // componentWillMount() {
    //     //     this.authenticationCheck(this.props);
    //     // }
    //     //
    //     // authenticationCheck(props) {
    //     //     let state = store.getState().session;
    //     //
    //     //     if(!state.loggedIn)
    //     //         props.history.push('/login');
    //     // }
    //
    //     render() {
    //         return <BaseComponent {...this.props} />;
    //     }
    // }
    //
    // CheckLoggedIn.propTypes = {
    //     location: PropTypes.object.isRequired
    // };

    if (authenticationCheck()) {
        return BaseComponent;
    } else {
        console.log('here');
        // window.location = '/login';
    }

};
