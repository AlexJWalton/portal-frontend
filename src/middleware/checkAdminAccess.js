import React, {Component} from 'react';
import PropTypes from 'prop-types';

import store from './../store';

export default (BaseComponent) => {
    class CheckAdminAccess extends Component
    {
        componentWillMount() {
            this.authenticationCheck(this.props);
        }

        authenticationCheck(props) {
            let user_type = store.getState().session.user_details.user_type;

            if(user_type != 'ADMIN')
                props.history.push('/404');
        }

        render() {
            return <BaseComponent {...this.props} />;
        }
    }

    CheckAdminAccess.propTypes = {
        location: PropTypes.object.isRequired
    };

    return CheckAdminAccess;

};
