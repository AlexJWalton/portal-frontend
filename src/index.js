import React from 'react';
import ReactDOM from 'react-dom';
import Router from './config/router';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import store from './store';

// Import Material CSS + JS files
import 'materialize-css/dist/js/materialize';
// import 'materialize-css/dist/css/materialize.css';
import './css/custom.css';

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#282c4e',
            main: '#000026',
            dark: '#000000',
            contrastText: '#fff',
        }
    },
});

ReactDOM.render(
    <MuiThemeProvider theme={theme}>
        <Provider store={store}>
            <Router />
        </Provider>
    </MuiThemeProvider>,
    document.getElementById('root'));
registerServiceWorker();
