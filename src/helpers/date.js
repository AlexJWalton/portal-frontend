import monthNames from '../data/months';

export default {
    correctDate(rawDate) {
        return new Date(rawDate.replace(" ","T"));
    },

    formatDate(date) {
        var day = date.getDate(),
            monthIndex = date.getMonth(),
            year = date.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    },

    formatTime(date) {
        var hour = date.getHours(),
            minute = date.getMinutes(),
            part = 'am';

        if(hour > 12) {
            hour -= 12;
            part = 'pm';
        }

        if (minute == 0) {
            minute = '00';
        }

        return hour + ':' + minute + ' ' + part;
    }
}
