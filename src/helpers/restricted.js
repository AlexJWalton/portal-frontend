import React, {Component} from 'react';
import PropTypes from 'prop-types';

import store from './../store';

export default (BaseComponent) => {
    class Restricted extends Component
    {
        componentWillMount() {
            this.authenticationCheck(this.props);
        }

        authenticationCheck(props) {
            let state = store.getState().session;

            if(!session.loggedIn)
                props.history.push('/login');
        }

        render() {
            return <BaseComponent {...this.props} />;
        }
    }

    Restricted.propTypes = {
        location: PropTypes.object.isRequired
    };

    return Restricted;
    
};
