import { combineReducers } from 'redux';

import session from './session';
import patrol from './patrol';
import user from './user';
import team from './team';
import modal from './modal';
import profile from './profile';
import {
    reducer as flashReducer,
} from 'redux-flash'

export default combineReducers({
    session,
    patrol,
    user,
    team,
    profile,
    flash: flashReducer,
    modal
})
