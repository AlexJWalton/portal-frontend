import UserService from '../services/userService';

const getInitialState = () => {
    var initialState = {
        current: {},
        all: []
    };

    // Initialise session data from session storage if it exists
    if (sessionStorage.getItem('users') !== null) {
        initialState = JSON.parse(sessionStorage.getItem('users'));
    }

    return initialState;
};

export default (state = getInitialState(), action) => {
    var newState = state;

    switch (action.type) {
        case 'SET_CURRENT_USER':
            newState.current = action.user;
            sessionStorage.setItem('users', JSON.stringify(newState));
            break;

        case 'UPDATE_CURRENT_USER_EMAIL':
            newState.current.email = action.email;
            sessionStorage.setItem('users', JSON.stringify(newState));
            break;

        case 'UPDATE_CURRENT_USER_FIRST_NAME':
            newState.current.first_name = action.firstName;
            sessionStorage.setItem('users', JSON.stringify(newState));
            break;

        case 'UPDATE_CURRENT_USER_LAST_NAME':
            newState.current.last_name = action.lastName;
            sessionStorage.setItem('users', JSON.stringify(newState));
            break;

        case 'ALL_USERS':
            newState.all = action.users;
            sessionStorage.setItem('users', JSON.stringify(newState));
            break;
    }

    return Object.assign({}, newState);
}
