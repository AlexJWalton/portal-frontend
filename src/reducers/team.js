import TeamService from '../services/teamService';

const getInitialState = () => {
    var initialState = {
        current: {
            members: []
        },
        all: []
    };

    // Initialise session data from session storage if it exists
    if (sessionStorage.getItem('teams') !== null) {
        initialState = JSON.parse(sessionStorage.getItem('teams'));
    }

    return initialState;
};

export default (state = getInitialState(), action) => {
    var newState = state;

    switch (action.type) {
        case 'SET_CURRENT_TEAM':
            newState.current = action.team;
            newState.current.members = action.teamMembers;
            sessionStorage.setItem('teams', JSON.stringify(newState));
            break;

        case 'ALL_TEAMS':
            newState.all = action.teams;
            sessionStorage.setItem('teams', JSON.stringify(newState));
            break;
    }

    return Object.assign({}, newState);
}
