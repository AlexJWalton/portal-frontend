import EventService from '../services/eventService';

const getInitialState = () => {
    var initialState = {
        current: {},
        all: []
    };

    // Initialise session data from session storage if it exists
    if (sessionStorage.getItem('patrols') !== null) {
        initialState = JSON.parse(sessionStorage.getItem('patrols'));
    }

    return initialState;
};

export default (state = getInitialState(), action) => {
    var newState = state;

    switch (action.type) {
        case 'SET_CURRENT_PATROL':
            newState.current = action.event;
            sessionStorage.setItem('patrols', JSON.stringify(newState));
            break;

        case 'ALL_PATROLS':
            newState.all = action.events;
            break;
    }

    return Object.assign({}, newState);
}
