const getInitialState = () => {
    var initialState = {
        firstName: '',
        lastName: '',
        email: ''
    };

    // Initialise session data from session storage if it exists
    if (sessionStorage.getItem('user.profile') !== null) {
        initialState = JSON.parse(sessionStorage.getItem('user.profile'));
    }

    return initialState;
};

export default (state = getInitialState(), action) => {
    var newState = state;

    switch (action.type) {
        case 'SET_PROFILE':
            newState = {
                id: action.id,
                firstName: action.firstName,
                lastName: action.lastName,
                email: action.email
            };

            sessionStorage.setItem('user.profile', JSON.stringify(newState));
            break;

        case 'UPDATE_PROFILE_FIRST_NAME':
            newState = {
                id: state.id,
                firstName: action.firstName,
                lastName: state.lastName,
                email: state.email
            };

            sessionStorage.setItem('user.profile', JSON.stringify(newState));
            break;

        case 'UPDATE_PROFILE_LAST_NAME':
            newState = {
                id: state.id,
                firstName: state.firstName,
                lastName: action.lastName,
                email: state.email
            };

            sessionStorage.setItem('user.profile', JSON.stringify(newState));
            break;

        case 'UPDATE_PROFILE_EMAIL':
            newState = {
                id: state.id,
                firstName: state.firstName,
                lastName: state.lastName,
                email: action.email
            };

            sessionStorage.setItem('user.profile', JSON.stringify(newState));
            break;
    }

    return Object.assign({}, newState);
}
