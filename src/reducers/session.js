const getInitialState = () => {
    var initialState = {
        loggedIn: false,
        access_token: false,
        email: false
    };

    // Initialise session data from session storage if it exists
    if (sessionStorage.getItem('user.session') !== null) {
        initialState = JSON.parse(sessionStorage.getItem('user.session'));
    }

    return initialState;
};

export default (state = getInitialState(), action) => {
    var newState = state;

    switch (action.type) {
        case 'USER_LOGGED_IN':
            newState = {
                loggedIn: true,
                access_token: action.access_token,
                email: action.email,
                id: action.id,
                user_details: action.user_details
            };

            sessionStorage.setItem('user.session', JSON.stringify(newState));
            break;

        case 'USER_LOGGED_OUT':
            newState = {
                loggedIn: false,
                access_token: false,
                email: false
            };
            sessionStorage.setItem('user.session', JSON.stringify(newState));
            break;
    }

    return Object.assign({}, newState);
}
