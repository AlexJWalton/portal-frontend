const getInitialState = () => {
    var initialState = {
        teamModalOpen: false
    };

    // Initialise session data from session storage if it exists
    if (sessionStorage.getItem('modal') !== null) {
        initialState = JSON.parse(sessionStorage.getItem('modals'));
    }

    return initialState;
};

export default (state = getInitialState(), action) => {
    var newState = state;

    switch (action.type) {
        case 'SET_TEAM_MODAL_OPEN':
            newState.teamModalOpen = true;
            sessionStorage.setItem('modals', JSON.stringify(newState));
            break;

        case 'SET_TEAM_MODAL_CLOSE':
            newState.teamModalOpen = false;
            sessionStorage.setItem('modals', JSON.stringify(newState));
            break;
    }

    return Object.assign({}, newState);
}
