import React from 'react';

export default props => (
    <div className="card-action">{props.children}</div>
);