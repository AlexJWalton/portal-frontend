import React from 'react';
import CardContent from '@material-ui/core/CardContent';

export default props => (
    <CardContent>{props.children}</CardContent>
);
