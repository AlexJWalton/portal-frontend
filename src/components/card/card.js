import React, { Component } from 'react';

import CardMUI from '@material-ui/core/Card';

class Card extends Component
{
    render() {

        // var cardClass = 'card';
        //
        // if(this.props.hoverable === true)
        //     cardClass = cardClass + ' hoverable';

        return (
            <CardMUI style={this.props.style}>
                {this.props.children}
            </CardMUI>
        );
    }
}

export default Card;
