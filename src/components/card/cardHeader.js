import React from 'react';

export default props => (
    <span className="card-title">{props.title}</span>
);