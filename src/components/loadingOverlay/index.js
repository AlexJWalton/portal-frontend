import React, {Component} from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';
import grey from '@material-ui/core/colors/grey';

let styles = {
    loading: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        background: 'rgba(0, 0, 0, 0.40)',
        textAlign: 'center',
        zIndex: 100,
        paddingTop: '20vh'
    }
}

export default props => (
    <div style={styles.loading}>
        <div style={styles.loadingItem}>
            <CircularProgress size={100} style={{ color: grey[100] }} thickness={4} />
        </div>
    </div>
);
