import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { flashMessageType } from 'redux-flash';

import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';

import green from '@material-ui/core/colors/green';

function FlashMessages ({flash}) {
    let styles = {
        position: 'absolute',
        bottom: '50px',
        right: '50px',
        zIndex: '1000'
    };

    if(flash && flash.message != '') {
        console.log(flash);
        return (
            <div style={styles}>
            {
                flash &&
                <SnackbarContent
                    style={{
                        backgroundColor: green[600]
                    }}
                    message={
                        <span id="client-snackbar" style={{display: 'flex', alignItems: 'center'}}>
                          {flash.message}
                        </span>
                    }
                />
            }
            </div>
        )
    }
    return (
        <div></div>
    )
}

FlashMessages.propTypes = {
    flash: PropTypes.flashMessageType
}

export default FlashMessages;
