import { connect } from 'react-redux';
import store from '../../store';
import { getLatestMessage } from 'redux-flash';

import FlashMessage from './flashMessage.component';

function mapStateToProps (state) {
    return {
        flash: getLatestMessage(state)
    }
}

export default connect(mapStateToProps)(FlashMessage)
