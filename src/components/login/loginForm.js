import React, { Component } from 'react';

class LoginForm extends Component
{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <label htmlFor="loginEmail">
                    Email
                    <input id="loginEmail" type="email"/>
                </label>
                <label htmlFor="loginPassword">
                    Password
                    <input id="loginPassword" type="password"/>
                </label>
                <button className="btn blue darken-4" style={{width: '100%'}}>Login</button>
            </div>
        );
    }
}

export default LoginForm;
