import React from 'react';

export default props => (
    <div style={{padding: '15px 24px'}}>
        {props.children}
    </div>
);
