import React, {Component} from 'react';

let styles = {
    fontSize: '1.5em',
    textTransform: 'uppercase',
    color: '#848484',
    padding: '20px 0px 15px',
    fontWeight: '300'
}

export default props => (
    <div style={styles}>
        {props.children}
    </div>
);
