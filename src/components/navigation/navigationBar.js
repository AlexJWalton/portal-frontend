import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import store from '../../store';

import env from '../../env';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Hidden from '@material-ui/core/Hidden';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

class NavigationBar extends Component
{
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            user_type : 'MEMBER'
        };
    }

    componentDidMount()
    {
        if(store.getState().session.user_details != undefined) {
            this.setState({
                user_type: store.getState().session.user_details.user_type
            });
        }
    }

    logout(event) {
        event.preventDefault();

        store.dispatch({
            type: 'USER_LOGGED_OUT'
        });

        window.location = '/login';
    }

    handleDrawerOpen() {
        console.log('here');
        this.setState({ open: true });
    }

    handleDrawerClose() {
        this.setState({ open: false });
    }

    render() {

        const styles = {
            root: {
                flexGrow: 1,
            },
            flex: {
                flex: 1,
            },
            header: {
                textDecoration: 'none',
                flex: 1,
            },
            menuButton: {
                marginLeft: -12,
                marginRight: 20,
            },
        };

        const PatrolsLink = props => <Link to="/patrols" {...props} />;
        const HomeLink = props => <Link to="/" {...props} />;
        const ProfileLink = props => <Link to="/profile" {...props} />;
        const AdminLink = props => <Link to="/admin" {...props} />;

        return (
            <AppBar position="sticky">
                <Toolbar>
                    <Hidden mdUp>
                        <IconButton color="inherit" aria-label="Menu" style={styles.menuButton} onClick={this.handleDrawerOpen.bind(this)}>
                            <MenuIcon />
                        </IconButton>
                        <Drawer open={this.state.open} onClose={this.handleDrawerClose.bind(this)}>
                            <div
                                tabIndex={0}
                                role="button"
                                onClick={this.handleDrawerClose.bind(this)}
                                onKeyDown={this.handleDrawerClose.bind(this)}
                                style={{width: '300px'}}
                            >
                            <List component="nav">
                                <ListItem button component={PatrolsLink}>
                                    <ListItemText primary="Patrol Roster" />
                                </ListItem>
                                <ListItem button component={ProfileLink}>
                                    <ListItemText primary="Profile" />
                                </ListItem>
                                {this.state.user_type == 'ADMIN' ? <ListItem button component={AdminLink}><ListItemText primary="Admin" /></ListItem> : ''}
                            </List>
                            <Divider />
                                <List component="nav">
                                    <ListItem button onClick={(e) => {this.logout(e)}}>
                                        <ListItemText primary="Logout" />
                                    </ListItem>
                                </List>
                            </div>
                        </Drawer>
                    </Hidden>
                    <Typography variant="title" color="inherit" style={styles.header}  component={HomeLink}>
                        Patrol Roster
                    </Typography>
                    <Hidden smDown>
                        <Button color="inherit" component={PatrolsLink}>Patrol Roster</Button>
                        <Button color="inherit" component={ProfileLink}>Profile</Button>
                        {this.state.user_type == 'ADMIN' ? <Button color="inherit" component={AdminLink}>Admin</Button> : ''}
                        <Button color="inherit" onClick={(e) => {this.logout(e)}}>Logout</Button>
                    </Hidden>
                </Toolbar>
            </AppBar>
        );
    }
}

export default NavigationBar;
