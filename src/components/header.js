import React from 'react';
import {Link} from 'react-router-dom';

import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Hidden from '@material-ui/core/Hidden';

import Container from './container/index';

let styles = {
    background: {
        backgroundColor: '#fff',
        borderBottom: '1px solid #d3dbe3'
    },
    text: {
        padding: '0.4em 0 0.4em 24px'
    },
    header: {
        textTransform: 'uppercase',
        fontWeight: '500',
        color: '#000'
    }
};

export default props => (
    <div style={styles.background}>
        <Container style={styles.text}>
            <Hidden mdUp>
                {props.backLink != null ? <div style={{display: 'inline-block'}}>
                    <IconButton onClick={props.backLinkAction} style={{top: '-3px', color: '#000'}}><Icon>chevron_left</Icon></IconButton>
                </div> : ''}
            </Hidden>
            <div style={{display: 'inline-block'}}>
                <h3 style={styles.header}>{props.title}</h3>
            </div>
            <Hidden smDown>
                <div style={{display: 'inline-block', float: 'right'}}>
                    {props.children}
                </div>
            </Hidden>
        </Container>
    </div>
);
