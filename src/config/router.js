import React, { Component } from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';

// Helpers
import restricted from '../helpers/restricted';

import store from './../store';

// Pages
import Login from '../pages/login';
import Signup from '../pages/signup/signup.component';
import Home from '../pages/home/home.connector';
import Roster from '../pages/patrolList/roster.connector';
import Patrol from '../pages/patrol/patrol.connector';
import Profile from '../pages/profile/profile.connector';
import Admin from '../pages/admin/admin.component';
import AddUser from '../pages/admin/users/add/addUser.connector';
import EditUser from '../pages/admin/users/edit/editUser.connector';
import User from '../pages/admin/users/show/user.connector';

import Team from '../pages/admin/teams/show/team.connector';

let checkLoggedIn = () => {
    let state = store.getState().session;
    return (state.loggedIn == true);
};

let checkAdminAccess = () => {
    if(checkLoggedIn()) {
        let user_type = store.getState().session.user_details.user_type;

        if(user_type == 'ADMIN') {
            return true;
        }
    }
    return false;
}

class Router extends Component {

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path='/login' component={Login}/>
                    <Route exact path='/register' component={Signup}/>

                    // Members Only
                    {checkLoggedIn() && (<Route exact path='/patrols' component={Roster}/>)}
                    {checkLoggedIn() && (<Route exact path='/profile' component={Profile}/>)}
                    {checkLoggedIn() && (<Route exact path='/patrols/:id' component={Patrol}/>)}

                    // Admins Only
                    {checkAdminAccess() && (<Route exact path='/admin' component={Admin}/>)}
                    {checkAdminAccess() && (<Route exact path='/admin/users' component={() => (<Redirect to="/admin"/>)}/>)}
                    {checkAdminAccess() && (<Route exact path='/admin/users/new' component={AddUser}/>)}
                    {checkAdminAccess() && (<Route exact path='/admin/users/:id' component={User}/>)}
                    {checkAdminAccess() && (<Route exact path='/admin/users/:id/edit' component={EditUser}/>)}
                    {checkAdminAccess() && (<Route exact path='/admin/teams' component={() => (<Redirect to="/admin"/>)}/>)}
                    {checkAdminAccess() && (<Route exact path='/admin/teams/:id' component={Team}/>)}

                    {checkLoggedIn() && (<Route exact path='*' component={Home}/>)}

                    <Route exact path='*' component={() => (<Redirect to="/login"/>)} />

                </Switch>
           </BrowserRouter>
        );
    }
}

export default Router;
