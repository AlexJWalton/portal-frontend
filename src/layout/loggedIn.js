import React, {Component} from 'react';

import NavigationBar from './../components/navigation/navigationBar';
import FlashMessage from './../components/FlashMessage/flashMessage.connector';

class LoggedIn extends Component
{
    render() {
        return(
            <div>
                <FlashMessage />
                <NavigationBar />
                {this.props.children}
            </div>
        );
    }
}

export default LoggedIn;
