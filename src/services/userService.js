import api from './apiV1Service';
import date from '../helpers/date';
import env from './../env';

import store from './../store';
import { flashMessage } from 'redux-flash';

export default {

    create(details) {
        details.client_id = env.clientId;
        details.client_secret = env.clientSecret;

        return api.post('register', details).then((res) => {

            store.dispatch(flashMessage('User successfully created'));

            return res;
        });
    },

    update(id, details) {
        return api.put('users/' + id, details).then((res) => {
            store.dispatch(flashMessage('User successfully updated'));
            return res;
        });
    },

    all() {
        return api.get('users').then((res) => {
            store.dispatch({
                type: 'ALL_USERS',
                users: res.data.data
            });
            return res;
        });
    },

    show(id) {
        return api.get('users/' + id);
    }
}
