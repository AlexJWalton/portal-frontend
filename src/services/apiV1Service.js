import 'whatwg-fetch';

import store from './../store';
import env from './../env';

export default {

    getHeaders() {
        let userSessionData = store.getState().session;

        return new Headers({
            'Content-Type' : 'application/json',
            "X-Auth-Client-Id": env.clientId,
            "X-Auth-User-Id": userSessionData.id,
            "Authorization": "Bearer " + userSessionData.access_token
        });
    },

    /**
     * Helper function to reduce amount of code needed to make GET requests
     * @param {string} url
     * @param {Object} extraParams
     * @returns {Promise}
     */
    get(url, extraParams = {})
    {
        return fetch(env.apiURL + url, {
            method: 'GET',
            headers: this.getHeaders(),
        }).then(response => {
            switch (response.status) {
                case 401:
                    return Promise.reject(response.statusText);
                default:
                    return response.json();
            }
        }).then(json => {
            if (json.error) {
                return Promise.reject(json.error.message);
            }
            return json;
        });
    },

    /**
     * Helper function to reduce amount of code needed to make POST requests
     *
     * @param {string} url
     * @param {FormData} parameters
     * @returns {Promise}
     */
    post(url, parameters)
    {
        return fetch(env.apiURL + url, {
            method: 'POST',
            headers: this.getHeaders(),
            body: JSON.stringify(parameters)
        }).then(response => {
            return response.json();
        });
    },

    /**
     * Helper function to reduce amount of code needed to make PUT requests
     *
     * @param {string} url
     * @param {FormData} parameters
     * @returns {Promise}
     */
    put(url, parameters)
    {
        parameters['_method'] = 'PUT';

        return this.post(url, parameters);
    },

    /**
     * Helper function to reduce amount of code needed to make DELETE requests
     *
     * @param {string} url
     * @param {FormData} parameters
     * @returns {Promise}
     */
    delete(url)
    {
        return fetch(env.apiURL + url, {
            method: 'DELETE',
            headers: this.getHeaders(),
        }).then(response => {
            switch (response.status) {
                case 401:
                    return Promise.reject(response.statusText);
                default:
                    return response.json();
            }
        }).then(json => {
            if (json.error) {
                return Promise.reject(json.error.message);
            }
            return json;
        });
    },

}
