import api from './apiV1Service';
import store from './../store';

export default {

    // Get all teams
    all() {
        return api.get('teams').then((res) => {
            store.dispatch({
                type: 'ALL_TEAMS',
                teams: res.data.data
            });
            return res;
        });
    },

    // Get a specific team by ID
    show(id) {
        return api.get('teams/' + id);
    },

    addMember(teamId, userId) {
        return api.post('teams/' + teamId + '/members', {
            user_id: userId
        });
    },

    // Create a team
    create(data) {
        return api.post('teams', null);
    },

    // Delete a team
    delete(id) {
        return api.delete('teams/' + id);
    }
};
