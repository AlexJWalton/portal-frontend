import api from './apiV1Service';
import store from './../store';
import env from './../env';

export default {

    // Authenticate user against the API
    login(email, password) {
        let data = {
            'email': email,
            'password': password,
            'client_id': env.clientId,
            'client_secret': env.clientSecret
        };
        return api.post('login', data);
    },

    register(email, password, firstName, lastName) {
        let data = {
            'email': email,
            'password': password,
            'client_id': env.clientId,
            'client_secret': env.clientSecret,
            'first_name': firstName,
            'last_name': lastName,
            'username': email
        }

        return api.post('register', data);
    },

    // Logout of the user account
    logout() {
        return false;
    }

};
