import api from './apiV1Service';
import date from '../helpers/date';

import store from './../store';
import { flashMessage } from 'redux-flash';

export default {

    // Get all events
    allCurrent() {
        return api.get('events?current=true').then((res) => {
            store.dispatch({
                type: 'ALL_PATROLS',
                events: res.data.data
            });
            return res;
        });
    },

    all() {
        return api.get('events').then((res) => {
            store.dispatch({
                type: 'ALL_PATROLS',
                events: res.data.data
            });
            return res;
        });
    },

    // Get a specific event by ID
    show(id) {
        return api.get('events/' + id).then((res) => {
            var attr = res.data.attributes;
            var teams = res.data.teams;
            var members = res.data.members;
            var substitutes = res.data.substitutes;
            var e = {}, m = [];

            // Set Event variables
            e = {
                startsAt: date.correctDate(attr.starts_at),
                startDate: date.formatDate(date.correctDate(attr.starts_at)),
                startTime: date.formatTime(date.correctDate(attr.starts_at)),
                endsAt: date.correctDate(attr.ends_at),
                endDate: date.formatDate(date.correctDate(attr.ends_at)),
                endTime: date.formatTime(date.correctDate(attr.ends_at)),
                type: attr.event_type,
                name: attr.event_name,
                activeInTeam: this.isActiveInTeam(res.data),
                activeAsMember: this.isActiveAsMember(res.data)
            }

            store.dispatch({
                type: 'SET_CURRENT_PATROL',
                event: {
                    id: id,
                    attributes: e,
                    members: this.prettifyMembers(members),
                    teams: this.prettifyTeams(teams, substitutes)
                }
            })

            return {
                event: e,
                members: m,
                teams: this.prettifyTeams(teams, substitutes)
            };
        });
    },

    // Create an event
    create(data) {
        return api.post('events', null);
    },

    // Delete an event
    delete(id) {
        return api.delete('events/' + id);
    },

    joinEvent(id) {
        return api.post('events/' + id + '/join').then((res) => {
            this.show(id);
            store.dispatch(flashMessage('You have joined the patrol'));
        });
    },

    leaveEvent(id) {
        return api.delete('events/' + id + '/leave').then((res) => {
            this.show(id);
            store.dispatch(flashMessage('You have left the patrol'));
        });
    },

    prettifyTeams(teams, substitutes)
    {
        var t = [];

        teams.map((team, index) => {
            var u = [];
            team.users.map((user, i) => {
                var s = {};

                substitutes.map((sub, i2) => {
                    if(sub.attributes.user_id == user.attributes.id) {
                        s = {
                            id: sub.substitute.attributes.id,
                            firstName: sub.substitute.attributes.first_name,
                            lastName: sub.substitute.attributes.last_name,
                        }
                    }
                });

                u[i] = {
                    id: user.attributes.id,
                    firstName: user.attributes.first_name,
                    lastName: user.attributes.last_name,
                    substitute: s
                };
            });

            t[index] = {
                id: team.attributes.id,
                name: team.attributes.name,
                members: u
            }
        });

        return t;
    },

    prettifyMembers(members) {
        var m = [];

        members.map((member, index) => {
            m[index] = {
                id: member.attributes.id,
                firstName: member.attributes.first_name,
                lastName: member.attributes.last_name
            }
        });

        return m;
    },

    isActiveInTeam(patrol) {
        var activeUser = store.getState().session;
        var active = false;

        patrol.teams.map((team, tIndex) => {
            team.users.map((teamMember, tmIndex) => {
                if(teamMember.id == activeUser.id) {
                    active = true;
                }
            });
        });

        return active;
    },

    isActiveAsMember(patrol) {
        var activeUser = store.getState().session;
        var active = false;

        patrol.members.map((member, mIndex) => {
            if(member.id == activeUser.id) {
                active = true;
            }
        });

        return active;
    },

    isActive(patrol) {
        return (this.isActiveInTeam(patrol) || this.isActiveAsMember(patrol));
    }
};
