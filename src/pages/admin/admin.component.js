import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

// Components
import LoggedInLayout from '../../layout/loggedIn';
import Container from '../../components/container/index';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import Users from './users/users.connector';
import Teams from './teams/teams.connector';

class Admin extends Component
{
    constructor(props) {
        super(props);

        this.state = {
            value: 0
        };
    }

    handleTabChange(event, v) {
        this.setState({ value: v });
    }

    render() {
        return (
            <LoggedInLayout>
                <Helmet>
                    <title>Admin | Club Portal</title>
                </Helmet>
                <Container style={{paddingTop: '15px'}}>
                    <AppBar position="static" style={{backgroundColor: '#fff', color: 'rgb(60, 60, 60)'}}>
                        <Tabs value={this.state.value} indicatorColor="primary" onChange={this.handleTabChange.bind(this)}>
                            <Tab label="Users" />
                            <Tab label="Teams" />
                            <Tab label="Patrols" />
                        </Tabs>
                    </AppBar>
                    {this.state.value === 0 && <Users />}
                    {this.state.value === 1 && <Teams />}
                    {this.state.value === 2 && <div>Item Three</div>}
                </Container>
            </LoggedInLayout>
        );
    }
}

export default Admin;
