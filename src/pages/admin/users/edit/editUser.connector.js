import { connect } from 'react-redux';
import store from '../../../../store';

import EditUser from './editUser.component';

import UserService from './../../../../services/userService';

function mapStateToProps(state, other) {
    return {
        firstName: store.getState().user.current.first_name,
        lastName: store.getState().user.current.last_name,
        email: store.getState().user.current.email,
        id: store.getState().user.current.id
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateFirstName: (e) => {
            store.dispatch({
                type: 'UPDATE_CURRENT_USER_FIRST_NAME',
                firstName: e.target.value
            });
        },
        updateLastName: (e) => {
            store.dispatch({
                type: 'UPDATE_CURRENT_USER_LAST_NAME',
                firstName: e.target.value
            });
        },
        updateEmail: (e) => {
            store.dispatch({
                type: 'UPDATE_CURRENT_USER_EMAIL',
                firstName: e.target.value
            });
        },
        updateUser(e) {
            e.preventDefault();
            UserService.update(
                store.getState().user.current.id,
                {
                    first_name: store.getState().user.current.first_name,
                    last_name: store.getState().user.current.last_name,
                    email: store.getState().user.current.email
                });
            window.location = '/admin/users/' + store.getState().user.current.id;
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditUser)
