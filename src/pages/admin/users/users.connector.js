import { connect } from 'react-redux';
import store from '../../../store';

import Users from './users.component';

import UserService from './../../../services/userService';

function mapStateToProps(state, other) {
    return {
        users: store.getState().user.all
    }
}

function mapDispatchToProps(dispatch) {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Users)
