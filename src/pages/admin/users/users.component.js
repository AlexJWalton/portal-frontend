import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import Hidden from '@material-ui/core/Hidden';

import UserService from '../../../services/userService';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FilterListIcon from '@material-ui/icons/FilterList';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import Icon from '@material-ui/core/Icon';

import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';

class Users extends Component
{
    componentDidMount() {
        UserService.all();
    }

    render() {

        const AddLink = props => <Link to="/admin/users/new" {...props} />;

        return (
            <div>
                <Paper style={{ width: '100%', marginTop: '20px', overflowX: 'auto' }}>
                    <Table style={{ minWidth: '700' }}>
                        <TableHead>
                            <TableRow>
                                <Hidden smDown>
                                    <TableCell>User ID</TableCell>
                                    <TableCell>First Name</TableCell>
                                    <TableCell>Last Name</TableCell>
                                    <TableCell>Email</TableCell>
                                    <TableCell>User Type</TableCell>
                                </Hidden>
                                <Hidden mdUp>
                                    <TableCell>Name</TableCell>
                                    <TableCell>User Type</TableCell>
                                </Hidden>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.users.map(u => {
                                return (
                                    <TableRow key={u.attributes.id}>
                                        <Hidden smDown>
                                            <TableCell component="th" scope="row">
                                                <Link to={'/admin/users/' + u.attributes.id} style={{color: 'inherit', textDecoration: 'inherit'}}>
                                                    {u.attributes.id}
                                                </Link>
                                            </TableCell>
                                            <TableCell>
                                                <Link to={'/admin/users/' + u.attributes.id} style={{color: 'inherit', textDecoration: 'inherit'}}>
                                                    {u.attributes.first_name}
                                                </Link>
                                            </TableCell>
                                            <TableCell>
                                                <Link to={'/admin/users/' + u.attributes.id} style={{color: 'inherit', textDecoration: 'inherit'}}>
                                                    {u.attributes.last_name}
                                                </Link>
                                            </TableCell>
                                            <TableCell>
                                                <Link to={'/admin/users/' + u.attributes.id} style={{color: 'inherit', textDecoration: 'inherit'}}>
                                                    {u.attributes.email}
                                                </Link>
                                            </TableCell>
                                            <TableCell>
                                                <Link to={'/admin/users/' + u.attributes.id} style={{color: 'inherit', textDecoration: 'inherit'}}>
                                                    {u.attributes.user_type}
                                                </Link>
                                            </TableCell>
                                        </Hidden>
                                        <Hidden mdUp>
                                            <TableCell>
                                                <Link to={'/admin/users/' + u.attributes.id} style={{color: 'inherit', textDecoration: 'inherit'}}>
                                                    {u.attributes.first_name + ' ' + u.attributes.last_name}
                                                </Link>
                                            </TableCell>
                                            <TableCell>
                                                <Link to={'/admin/users/' + u.attributes.id} style={{color: 'inherit', textDecoration: 'inherit'}}>
                                                    {u.attributes.user_type}
                                                </Link>
                                            </TableCell>
                                        </Hidden>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </Paper>
                <Button
                    variant="fab"
                    style={{backgroundColor: '#9e0000', color: '#ffffff', position: 'fixed', bottom: '30px', right: '30px'}}
                    aria-label="add"
                    component={AddLink}
                >
                    <AddIcon />
                </Button>
            </div>
        );
    }
}

Users.propTypes = {
    users: PropTypes.array
}

export default Users;
