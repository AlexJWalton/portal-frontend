import { connect } from 'react-redux';
import store from '../../../../store';

import User from './user.component';

import UserService from './../../../../services/userService';

function mapStateToProps(state, other) {
    return {
        user: store.getState().user.current,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setUser(id) {
            UserService.show(id).then((res) => {
                store.dispatch({
                    type: 'SET_CURRENT_USER',
                    user: res.data.attributes
                })
            });
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(User)
