import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';

// Layout
import LoggedInLayout from '../../../../layout/loggedIn';

// Components
import Loading from '../../../../components/loadingOverlay/index';
import PatrolHeader from '../../../../components/header';
import Container from '../../../../components/container/index';
import Card from '../../../../components/card/card';
import CardContent from '../../../../components/card/cardContent';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

class User extends Component
{
    componentDidMount() {
        this.props.setUser(this.props.match.params.id);
        console.log(this.props.user);
    }

    render() {
        const EditLink = props => <Link to={"/admin/users/" + this.props.match.params.id + "/edit"} {...props} />;
        const CancelLink = props => <Link to={"/admin"} {...props} />;

        return (
            <LoggedInLayout>
                <Helmet>
                    <title>Admin - { this.props.user.first_name + ' ' + this.props.user.last_name } | Patrol Roster</title>
                </Helmet>
                <PatrolHeader
                    title={ this.props.user.first_name + ' ' + this.props.user.last_name }
                    backLink={true}
                    backLinkAction={() => {this.props.history.push('/admin')}}
                >
                    <Button
                        style={{marginTop: '10px'}}
                        component={CancelLink}
                    >
                        Back to Admin Page
                    </Button>
                    <Button
                        variant="contained"
                        style={{marginTop: '10px', marginLeft: '15px', backgroundColor: 'rgb(243, 243, 243)', color: '#000'}}
                        component={EditLink}
                    >
                        Edit
                    </Button>
                </PatrolHeader>
                <Container>
                    <Card>
                        <CardContent>
                            <TextField
                                id="first_name"
                                label="First Name"
                                value={this.props.user.first_name}
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                                margin="normal"
                                disabled={true}
                            />
                            <TextField
                                id="last_name"
                                label="Last Name"
                                value={this.props.user.last_name}
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                                margin="normal"
                                disabled={true}
                            />
                            <TextField
                                id="email"
                                label="Email"
                                value={this.props.user.email}
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                                margin="normal"
                                disabled={true}
                            />
                            <TextField
                                id="user-type"
                                label="User Type"
                                value={this.props.user.user_type}
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                                margin="normal"
                                disabled={true}
                            />
                        </CardContent>
                    </Card>
                    <Card style={{marginTop: '25px'}}>
                        <CardContent>
                            <TextField
                                id="created-at"
                                label="Created At"
                                value={this.props.user.created_at}
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                                margin="normal"
                                disabled={true}
                            />
                            <TextField
                                id="updated-at"
                                label="Last Updated"
                                value={this.props.user.updated_at}
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                                margin="normal"
                                disabled={true}
                            />
                        </CardContent>
                    </Card>
                </Container>
            </LoggedInLayout>
        );
    }
}

User.PropTypes = {
    firstName: PropTypes.String,
    lastName: PropTypes.String,
    email: PropTypes.String
};

export default User;
