import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';

// Layout
import LoggedInLayout from '../../../../layout/loggedIn';

// Components
import Loading from '../../../../components/loadingOverlay/index';
import PatrolHeader from '../../../../components/header';
import Container from '../../../../components/container/index';
import Card from '../../../../components/card/card';
import CardContent from '../../../../components/card/cardContent';
import Header from '../../../../components/header/index';

import AddMember from './components/addMember.connector';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon';

class Team extends Component
{
    componentDidMount() {
        this.props.setTeam(this.props.match.params.id);
    }

    render() {
        const EditLink = props => <Link to={"/admin/teams/" + this.props.match.params.id + "/edit"} {...props} />;
        const CancelLink = props => <Link to={"/admin"} {...props} />;

        return (
            <LoggedInLayout>
                <Helmet>
                    <title>Admin - {this.props.team.name + ''} | Patrol Roster</title>
                </Helmet>
                <PatrolHeader
                    title={this.props.team.name}
                    backLink={true}
                    backLinkAction={() => {this.props.history.push('/admin')}}
                >
                    <Button
                        style={{marginTop: '10px'}}
                        component={CancelLink}
                    >
                        Back to Admin Page
                    </Button>
                    <Button
                        variant="contained"
                        style={{marginTop: '10px', marginLeft: '15px', backgroundColor: 'rgb(243, 243, 243)', color: '#000'}}
                        component={EditLink}
                    >
                        Edit
                    </Button>
                </PatrolHeader>
                <Container>
                    <Card>
                        <CardContent>
                            <TextField
                                id="team_name"
                                label="Team Name"
                                value={this.props.team.name}
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                                margin="normal"
                                disabled={true}
                            />
                        </CardContent>
                    </Card>
                    <Card style={{marginTop: '25px'}}>
                        <CardContent>
                            <TextField
                                id="created-at"
                                label="Created At"
                                value={this.props.team.created_at}
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                                margin="normal"
                                disabled={true}
                            />
                            <TextField
                                id="updated-at"
                                label="Last Updated"
                                value={this.props.team.updated_at}
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                                margin="normal"
                                disabled={true}
                            />
                        </CardContent>
                    </Card>


                    <div style={{marginTop: '25px'}}>
                        <Header>Members</Header>
                        <Paper style={{ width: '100%', overflowX: 'auto' }}>
                            <Table style={{ minWidth: '700' }}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>User ID</TableCell>
                                        <TableCell>First Name</TableCell>
                                        <TableCell>Last Name</TableCell>
                                        <TableCell></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.props.team.members.map(u => {
                                        return (
                                            <TableRow key={u.attributes.id}>
                                                <TableCell component="th" scope="row">
                                                    {u.attributes.id}
                                                </TableCell>
                                                <TableCell>
                                                    {u.attributes.first_name}
                                                </TableCell>
                                                <TableCell>
                                                    {u.attributes.last_name}
                                                </TableCell>
                                                <TableCell>
                                                    <Icon>delete</Icon>
                                                </TableCell>
                                            </TableRow>
                                        );
                                    })}
                                </TableBody>
                            </Table>
                            <AddMember />
                        </Paper>
                    </div>
                </Container>
            </LoggedInLayout>
        );
    }
}

Team.PropTypes = {
    // team: PropTypes.array
};

export default Team;
