import { connect } from 'react-redux';
import store from '../../../../../store';

import AddMember from './addMember.component';

import TeamService from '../../../../../services/teamService';
import UserService from '../../../../../services/userService';

function mapStateToProps(state, other) {
    return {
        team: store.getState().team.current,
        users: store.getState().user.all
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getUsers(id) {
            UserService.all();
        },
        handleSubmit() {
            let element = document.getElementById("member-select");
            TeamService.addMember(store.getState().team.current.id, element.value).then(res => {
                TeamService.show(store.getState().team.current.id).then((r) => {
                    store.dispatch({
                        type: 'SET_CURRENT_TEAM',
                        team: r.data.attributes,
                        teamMembers: r.data.users
                    });
                });
            });

        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddMember)
