import React, { Component } from 'react';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon';

import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

class AddMember extends Component
{
    render() {
        return (
            <div style={{margin: '10px'}}>
                <Select
                    native
                    name="addMember"
                    id="member-select"
                    inputProps={{name: 'age'}}
                >
                    <option value=""></option>
                    {this.props.users.map(u => {
                        var found = false;
                        this.props.team.members.map(tm => {
                            if(found == false && tm.attributes.id == u.attributes.id) {
                                found = true;
                            }
                        });
                        if(!found) {
                            return (
                                <option value={u.attributes.id} key={u.attributes.id}>
                                    {u.attributes.first_name + ' ' + u.attributes.last_name}
                                </option>
                            );
                        }
                    })}
                </Select>
                <Button onClick={() => {this.props.handleSubmit()}} color="primary">
                    Add
                </Button>
            </div>
        );
    }
}

AddMember.PropTypes = {
    // team: PropTypes.array
};

export default AddMember;
