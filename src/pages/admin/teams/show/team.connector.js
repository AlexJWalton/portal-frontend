import { connect } from 'react-redux';
import store from '../../../../store';

import Team from './team.component';

import TeamService from '../../../../services/teamService';

function mapStateToProps(state, other) {
    return {
        team: store.getState().team.current,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setTeam(id) {
            TeamService.show(id).then((res) => {
                store.dispatch({
                    type: 'SET_CURRENT_TEAM',
                    team: res.data.attributes,
                    teamMembers: res.data.users
                })
            });
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Team)
