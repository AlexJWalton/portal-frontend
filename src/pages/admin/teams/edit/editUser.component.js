import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';

// Layout
import LoggedInLayout from '../../../../layout/loggedIn';

// Components
import Loading from '../../../../components/loadingOverlay/index';
import PatrolHeader from '../../../../components/header';
import Container from '../../../../components/container/index';
import Card from '../../../../components/card/card';
import CardContent from '../../../../components/card/cardContent';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';


class EditUser extends Component
{
    constructor(props) {
        super(props);
    }

    render() {
        const CancelLink = props => <Link to={'/admin/users/' + this.props.id} {...props} />;

        return (
            <LoggedInLayout>
                <Helmet>
                    <title>Edit User - { this.props.firstName + ' ' + this.props.lastName } | Patrol Roster</title>
                </Helmet>
                <PatrolHeader
                    title={'Edit User - ' + this.props.firstName + ' ' + this.props.lastName }
                >
                    <Button
                        variant="contained"
                        style={{marginTop: '10px', backgroundColor: 'rgb(243, 243, 243)', color: '#000'}}
                        component={CancelLink}
                    >
                        Cancel
                    </Button>
                    <Button
                        variant="contained"
                        style={{marginTop: '10px', marginLeft: '15px', backgroundColor: '#017002', color: '#ffffff'}}
                        onClick={(e) => {this.props.updateUser(e)}}
                    >
                        Save
                    </Button>
                </PatrolHeader>
                <Container>
                    <Card>
                        <CardContent>
                            <TextField
                                id="first_name"
                                label="First Name"
                                value={this.props.firstName}
                                onChange={(e) => {this.props.updateFirstName(e)}}
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                                margin="normal"
                            />
                            <TextField
                                id="last_name"
                                label="Last Name"
                                value={this.props.lastName}
                                onChange={(e) => {this.props.updateLastName(e)}}
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                                margin="normal"
                            />
                            <TextField
                                id="email"
                                label="Email"
                                value={this.props.email}
                                onChange={(e) => {this.props.updateEmail(e)}}
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                                margin="normal"
                            />
                        </CardContent>
                    </Card>
                </Container>
            </LoggedInLayout>
        );
    }
}

export default EditUser;
