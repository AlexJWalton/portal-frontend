import { connect } from 'react-redux';
import store from '../../../store';

import Teams from './teams.component';

import TeamService from './../../../services/teamService';

function mapStateToProps(state, other) {
    return {
        teams: store.getState().team.all
    }
}

function mapDispatchToProps(dispatch) {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Teams)
