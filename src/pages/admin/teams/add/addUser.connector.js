import { connect } from 'react-redux';
import store from '../../../../store';

import AddUser from './addUser.component';

import UserService from './../../../../services/userService';

var state = {
    firstName: '',
    lastName: '',
    email: '',
    password: ''
}

function mapStateToProps(state, other) {
    return {
        firstName: state.firstName,
        lastName: state.lastName,
        email: state.email,
        password: state.password
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateFirstName: (e) => {
            state.firstName = e.target.value;
        },
        updateLastName: (e) => {
            state.lastName = e.target.value;
        },
        updateEmail: (e) => {
            state.email = e.target.value;
        },
        updatePassword: (e) => {
            state.password = e.target.value;
        },
        createUser(e) {
            e.preventDefault();
            UserService.create({
                first_name: state.firstName,
                last_name: state.lastName,
                user_name: state.firstName + '.' + state.lastName,
                email: state.email,
                password: state.password
            })
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddUser)
