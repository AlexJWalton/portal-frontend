import React, { Component } from 'react';
import { connect } from 'react-redux';
import store from '../../store';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';

import env from '../../env';

// Components
import Card from '../../components/card/card';
import CardContent from '../../components/card/cardContent';
import LoginForm from '../../components/login/loginForm';
import Loading from '../../components/loadingOverlay/index';

import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

// Services
import Auth from '../../services/authService';

class Signup extends Component
{
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            loading: false
        };
    }

    updateEmail(event) {
        this.setState({
            email: event.target.value
        });
    }

    updatePassword(event) {
        this.setState({
            password: event.target.value
        });
    }

    updateFirstName(event) {
        this.setState({
            firstName: event.target.value
        });
    }

    updateLastName(event) {
        this.setState({
            lastName: event.target.value
        });
    }

    submitSignup(event) {
        // Prevent the default form action
        event.preventDefault();

        this.setState({
            loading: true
        });

        Auth.register(this.state.email, this.state.password, this.state.firstName, this.state.lastName)
            .then(res => {
                if(typeof res.data.id != 'undefined') {
                    this.props.history.push('/');
                } else {
                    this.setState({
                        loading: false
                    });
                }
            });
    }

    render() {
        return(
            <div>
                <Helmet>
                    <title>Register | {env.siteName}</title>
                </Helmet>
                {this.state.loading ? <Loading /> : <div />}
                <Grid container alignContent="center" justify="center" className="center-block" style={{maxWidth: '500px', margin: '0 auto', marginTop: '25px'}}>
                    <Grid item>
                        <h1 style={{textAlign: 'center'}}>{env.siteName} - Register</h1>
                        <Card>
                            <CardContent>
                                <div>
                                    <TextField
                                        id="registerEmail"
                                        label="Email"
                                        value={this.state.email}
                                        onChange={this.updateEmail.bind(this)}
                                        fullWidth
                                        margin="normal"
                                    />
                                    <TextField
                                        id="registerPassword"
                                        label="Password"
                                        value={this.state.password}
                                        onChange={this.updatePassword.bind(this)}
                                        fullWidth
                                        type="password"
                                        margin="normal"
                                    />
                                    <TextField
                                        id="registerFirstName"
                                        label="First Name"
                                        value={this.state.firstName}
                                        onChange={this.updateFirstName.bind(this)}
                                        fullWidth
                                        margin="normal"
                                    />
                                    <TextField
                                        id="registerLastName"
                                        label="Last Name"
                                        value={this.state.lastName}
                                        onChange={this.updateLastName.bind(this)}
                                        fullWidth
                                        margin="normal"
                                    />
                                    <Button
                                        variant="contained"
                                        fullWidth
                                        style={{marginTop: '10px', backgroundColor: '#017002', color: '#ffffff'}}
                                        onClick={this.submitSignup.bind(this)}
                                    >
                                        Register
                                    </Button>
                                    <Divider style={{margin: '25px 0'}} />
                                        <div>
                                            Already have an account? <Link to="/login"> Click here.</Link>
                                        </div>
                                </div>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default connect()(Signup)
