import { connect } from 'react-redux';
import store from '../../store';

import Roster from './roster.component';

function mapStateToProps(state, other) {
    return {
        events: store.getState().patrol.all
    }
}

export default connect(mapStateToProps)(Roster)
