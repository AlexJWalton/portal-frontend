import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import Card from '../../../components/card/card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '../../../components/card/cardContent';
import Chip from '@material-ui/core/Chip';

import Grid from '@material-ui/core/Grid';

import date from './../../../helpers/date';

let styles = {
    link: {
        color: 'inherit',
        textDecoration: 'inherit'
    },
    active: {
        background: 'rgba(22, 255, 19, 0.26)',
        height: '200px'
    }
};

export default props => (
    <Grid cols={1} item style={{width: '50%', minWidth: '350px'}}>
        <Link to={'/patrols/' + props.id} style={styles.link}>
            <Card hoverable={true} style={props.active ? styles.active : {height: '200px'}}>
                <CardHeader title={date.formatDate(props.startDate)} style={{paddingBottom: 0}}/>
                <CardContent active={true}  style={{paddingTop: 0, height: '225px'}}>
                    <p><b>Start Time:</b> {date.formatTime(props.startDate)}</p>
                    <p><b>End Time:</b> {date.formatTime(props.endDate)}</p>
                    <p><b>Teams: </b>
                    {props.teams.map((team, i) => {
                        if(i == 0)
                            return (<span key={i}>{team.attributes.name}</span>);
                        return (<span key={i}>{', ' + team.attributes.name}</span>);
                    })}
                    </p>
                </CardContent>
            </Card>
        </Link>
    </Grid>
);
