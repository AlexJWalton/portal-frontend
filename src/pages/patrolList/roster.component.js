import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Helmet} from 'react-helmet';

// Components
import LoggedInLayout from '../../layout/loggedIn';
import Loading from '../../components/loadingOverlay/index';
import PatrolListItem from './components/patrolListItem';
import Container from '../../components/container/index';
import Header from '../../components/header/index';
import PageHeader from '../../components/header';

// Helpers and Services
import date from '../../helpers/date';
import EventService from '../../services/eventService';

import Grid from '@material-ui/core/Grid';

class Roster extends Component
{
    constructor(props)
    {
        super(props);
    }

    componentDidMount()
    {
        EventService.allCurrent();
    }

    render() {

        if(this.props.events.length == 0) {
            return (
                <LoggedInLayout>
                    <Helmet>
                        <title>Patrol Roster | Club Portal</title>
                    </Helmet>
                    <PageHeader
                        title="Patrol Roster"
                    />
                    <Container style={{paddingTop: '15px'}}>
                        No upcoming patrols!
                    </Container>
                </LoggedInLayout>
            );
        }

        return (
            <LoggedInLayout>
                <Helmet>
                    <title>Patrol Roster | Club Portal</title>
                </Helmet>
                <PageHeader
                    title="Patrol Roster"
                />
                <Container style={{paddingTop: '15px'}}>
                    <Grid container justify="center" spacing={Number(16)}>
                        {this.props.events.map((patrol, index) => {
                            return (
                                <PatrolListItem
                                    key={index}
                                    id={patrol.id}
                                    startDate={date.correctDate(patrol.attributes.starts_at)}
                                    endDate={date.correctDate(patrol.attributes.ends_at)}
                                    teams={patrol.teams}
                                    active={EventService.isActive(patrol)}
                                />
                            );
                        })}
                    </Grid>
                </Container>
            </LoggedInLayout>
        );
    }
}

Roster.propTypes = {
    events: PropTypes.array
}

export default Roster;
