import { connect } from 'react-redux';
import store from '../../store';

import Profile from './profile.component';

import UserService from './../../services/userService';

function mapStateToProps(state, other) {
    return {
        firstName: store.getState().profile.firstName,
        lastName: store.getState().profile.lastName,
        email: store.getState().profile.email
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateFirstName: (e) => {
            store.dispatch({
                type: 'UPDATE_PROFILE_FIRST_NAME',
                firstName: e.target.value
            })
        },
        updateLastName: (e) => {
            store.dispatch({
                type: 'UPDATE_PROFILE_LAST_NAME',
                lastName: e.target.value
            })
        },
        updateEmail: (e) => {
            store.dispatch({
                type: 'UPDATE_PROFILE_EMAIL',
                email: e.target.value
            })
        },
        updateDetails: (e) => {
            e.preventDefault();

            let profile = {
                first_name: store.getState().profile.firstName,
                last_name: store.getState().profile.lastName,
            };
            UserService.update(store.getState().profile.id, profile);
        },
        updateProfileEmail: (e) => {
            e.preventDefault();
            let profile = {
                email: store.getState().profile.email
            };
            UserService.update(store.getState().profile.id, profile);
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Profile)
