import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {Helmet} from 'react-helmet';

// Layout
import LoggedInLayout from '../../layout/loggedIn';

// Components
import Loading from '../../components/loadingOverlay/index';
import Container from '../../components/container/index';
import PageHeader from '../../components/header';
import Card from '../../components/card/card';
import CardContent from '../../components/card/cardContent';
import Header from '../../components/header/index';

import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

// Services and helpers

class Profile extends Component
{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <LoggedInLayout>
                <Helmet>
                    <title>My Profile | Club Portal</title>
                </Helmet>
                <PageHeader
                    title="My Profile"
                />
                <Container style={{paddingTop: '15px'}}>
                    <Header>Update Details</Header>
                    <Card>
                        <CardContent>
                            <TextField
                                id="first_name"
                                label="First Name"
                                value={this.props.firstName}
                                onChange={(e) => {this.props.updateFirstName(e)}}
                                fullWidth
                                margin="normal"
                            />
                            <TextField
                                id="last_name"
                                label="Last Name"
                                value={this.props.lastName}
                                onChange={(e) => {this.props.updateLastName(e)}}
                                fullWidth
                                margin="normal"
                            />
                            <Button
                                variant="contained"
                                style={{marginTop: '10px', backgroundColor: '#017002', color: '#ffffff'}}
                                onClick={(e) => {this.props.updateDetails(e)}}
                            >
                                Save Details
                            </Button>
                        </CardContent>
                    </Card>
                    <Header>Update Email</Header>
                    <Card>
                        <CardContent>
                            <TextField
                                id="email"
                                label="Email"
                                value={this.props.email}
                                onChange={(e) => {this.props.updateEmail(e)}}
                                fullWidth
                                margin="normal"
                            />
                            <Button
                                variant="contained"
                                style={{marginTop: '10px', backgroundColor: '#017002', color: '#ffffff'}}
                                onClick={(e) => {this.props.updateProfileEmail(e)}}
                            >
                                Update Email
                            </Button>
                        </CardContent>
                    </Card>
                </Container>
            </LoggedInLayout>
        );
    }
}

Profile.propTypes = {
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    email: PropTypes.string
}

export default Profile;
