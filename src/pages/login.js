import React, { Component } from 'react';
import { connect } from 'react-redux';
import store from './../store';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';

import env from '../env';

// Components
import Card from '../components/card/card';
import CardContent from '../components/card/cardContent';
import LoginForm from '../components/login/loginForm';
import Loading from '../components/loadingOverlay/index';

import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

import { flashMessage } from 'redux-flash';

// Services
import Auth from '../services/authService';

class Login extends Component
{
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            loading: false,
            errorMessage: ''
        };
    }

    updateEmail(event) {
        this.setState({
            email: event.target.value
        });
    }

    updatePassword(event) {
        this.setState({
            password: event.target.value
        });
    }

    submitLogin(event) {
        // Prevent the default form action
        event.preventDefault();

        this.setState({
            loading: true
        });

        Auth.login(this.state.email, this.state.password)
            .then(res => {
                if(typeof res.data.error == 'undefined') {
                    store.dispatch({
                        type: 'USER_LOGGED_IN',
                        access_token: res.data.attributes.access_token,
                        email: res.data.attributes.email,
                        id: res.data.attributes.id,
                        user_details: res.data.attributes
                    });
                    store.dispatch({
                        type: 'SET_PROFILE',
                        id: res.data.attributes.id,
                        firstName: res.data.attributes.first_name,
                        lastName: res.data.attributes.last_name,
                        email: res.data.attributes.email
                    });
                    store.dispatch(flashMessage('Welcome!'));
                    window.location = '/';
                } else {
                    this.setState({
                        loading: false,
                        errorMessage: res.data.error.error_message
                    });
                }
            });
    }

    render() {

        const RegisterLink = props => <Link to="/register" {...props} />;

        return(
            <div>
                <Helmet>
                    <title>Login | {env.siteName}</title>
                </Helmet>
                {this.state.loading ? <Loading /> : <div />}
                <Grid container alignContent="center" justify="center" className="center-block" style={{maxWidth: '500px', margin: '0 auto', marginTop: '25px'}}>
                    <Grid item>
                        <h1 style={{textAlign: 'center'}}>{env.siteName} - Login</h1>
                        <Card>
                            <CardContent>
                                <div>
                                    <TextField
                                        id="loginEmail"
                                        label="Email"
                                        value={this.props.email}
                                        onChange={this.updateEmail.bind(this)}
                                        fullWidth
                                        margin="normal"
                                    />
                                    <TextField
                                        id="loginPassword"
                                        label="Password"
                                        type="password"
                                        value={this.props.password}
                                        onChange={this.updatePassword.bind(this)}
                                        fullWidth
                                        margin="normal"
                                    />
                                    <Button
                                        variant="contained"
                                        fullWidth
                                        style={{marginTop: '10px', backgroundColor: '#017002', color: '#ffffff'}}
                                        onClick={this.submitLogin.bind(this)}
                                    >
                                        Login
                                    </Button>
                                    <div style={{margin: '15px 0'}}>
                                        Forgot Password? <a href="/forgot-password"> Click here.</a>
                                    </div>
                                    {this.state.errorMessage != ''
                                        ? <div
                                            style={{
                                                padding: '10px',
                                                fontSize: '14px',
                                                background: '#b50000',
                                                borderRadius: '4px',
                                                color: '#fff',
                                                marginBottom: '10px'
                                            }}
                                        >
                                            {this.state.errorMessage}
                                        </div>
                                        : ''}
                                    <Divider style={{marginBottom: '25px 0'}} />
                                    <div>
                                        <Button
                                            variant="outlined"
                                            component={RegisterLink}
                                            style={{width: '100%', marginTop: '20px'}}
                                        >
                                            Signup
                                        </Button>
                                    </div>
                                </div>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default connect()(Login)
