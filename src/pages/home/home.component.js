import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Helmet} from 'react-helmet';

// Components
import LoggedInLayout from '../../layout/loggedIn';
import Container from '../../components/container/index';
import UpcomingPatrols from './components/upcomingPatrols';

import EventService from '../../services/eventService';

class Home extends Component
{
    componentDidMount()
    {
        EventService.allCurrent();
    }

    render() {


        return(
            <LoggedInLayout>
                <Helmet>
                    <title>Home | Club Portal</title>
                </Helmet>
                <Container>
                    {this.props.events != null ? <UpcomingPatrols {...this.props} /> : ""}
                </Container>
            </LoggedInLayout>
        );
    }
}

Home.propTypes = {
    events: PropTypes.array
}

export default Home;
