import { connect } from 'react-redux';
import store from '../../store';

import Home from './home.component';
import EventService from '../../services/eventService';

function mapStateToProps(state, other) {
    return {
        events: store.getState().patrol.all
    }
}

function mapDispatchToProps(dispatch) {
    return {
        joinPatrol: (e) => {
            EventService.joinEvent(store.getState().patrol.current.id)
        },
        leavePatrol: (e) => {
            EventService.leaveEvent(store.getState().patrol.current.id)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
