import React from 'react';

import Header from '../../../components/header/index';
import PatrolListItem from '../../patrolList/components/patrolListItem';

import EventService from '../../../services/eventService';
import date from '../../../helpers/date';

import Grid from '@material-ui/core/Grid';

export default props => (
    <div>
        <Header>My Upcoming Patrols</Header>
        <Grid container justify="center" spacing={Number(16)}>
            {props.events.map((patrol, index) => {
                if(EventService.isActive(patrol)) {
                    return (
                        <PatrolListItem
                            key={index}
                            id={patrol.id}
                            startDate={date.correctDate(patrol.attributes.starts_at)}
                            endDate={date.correctDate(patrol.attributes.ends_at)}
                            teams={patrol.teams}
                        />
                    );
                }
            })}
        </Grid>
    </div>
)
