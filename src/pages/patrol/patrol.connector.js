import { connect } from 'react-redux';
import store from '../../store';

import Patrol from './patrol.component';
import EventService from '../../services/eventService';

function mapStateToProps(state, other) {
    return {
        event: store.getState().patrol.current
    }
}

function mapDispatchToProps(dispatch) {
    return {
        joinPatrol: (e) => {
            EventService.joinEvent(store.getState().patrol.current.id)
        },
        leavePatrol: (e) => {
            EventService.leaveEvent(store.getState().patrol.current.id)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Patrol)
