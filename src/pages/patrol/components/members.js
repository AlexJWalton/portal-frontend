import React from 'react';

import Card from '../../../components/card/card';
import CardContent from '../../../components/card/cardContent';
import Header from '../../../components/header/index';

import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';

export default props => (
    <div>
        <Header>Members</Header>
        <Card>
            <CardContent>
                <div style={{marginTop: '15px'}}>
                    {props.event.members.length == 0 ? "No members" : ""}
                    {props.event.members.map((member, index) => {
                        return (
                            <Chip
                                key={index}
                                style={{margin: '0 5px 5px 0'}}
                                avatar={<Avatar>{member.firstName[0]}{member.lastName[0]}</Avatar>}
                                label={member.firstName + " " + member.lastName.toUpperCase()}
                            />
                        );
                    })}
                </div>
            </CardContent>
        </Card>
    </div>
);
