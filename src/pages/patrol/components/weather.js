import React from 'react';

import Card from '../../../components/card/card';
import CardContent from '../../../components/card/cardContent';

export default props => (
    <Card>
        <CardContent>
            <div style={{marginBottom: '15px'}}>
                <b>High Tide:</b> 14:23
            </div>
            <div style={{marginBottom: '15px'}}>
                <b>Low Tide:</b> 09:30
            </div>
            <div style={{marginBottom: '15px'}}>
                <b>Wind:</b> 14 km South
            </div>
            <div>
                <b>Weather:</b> Sunny
            </div>
        </CardContent>
    </Card>
);
