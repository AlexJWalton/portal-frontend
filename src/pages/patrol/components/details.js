import React from 'react';

import Card from '../../../components/card/card';
import CardContent from '../../../components/card/cardContent';
import Header from '../../../components/header/index';

export default props => (
    <div>
        <Header>Details</Header>
        <Card>
            <CardContent>
                <div style={{marginBottom: '15px'}}>
                    <b>Start Time:</b> {props.event.attributes.startTime}
                </div>
                <div style={{marginBottom: '15px'}}>
                    <b>End Time:</b> {props.event.attributes.endTime}
                </div>
                <div>
                    <b>Patrol Type:</b> {props.event.attributes.type}
                </div>
            </CardContent>
        </Card>
    </div>
);
