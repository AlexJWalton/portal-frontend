import React from 'react';

import Card from '../../../components/card/card';
import CardContent from '../../../components/card/cardContent';
import Header from '../../../components/header/index';

import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';

export default props => (
    <div>
        <Header>Teams</Header>
        <Card>
            <CardContent>
                <div>
                    {props.event.teams.length == 0 ? "No teams" : ""}
                    {props.event.teams.map((team, index) => {
                        return (
                            <div key={index}>
                                <h5>{team.name}</h5>
                                <div style={{paddingLeft: '15px'}}>
                                    {team.members.map((member, i) => {
                                        return (
                                            <Chip
                                                key={i}
                                                style={{margin: '0 5px 5px 0'}}
                                                avatar={<Avatar>{member.firstName[0]}{member.lastName[0]}</Avatar>}
                                                label={member.firstName + " " + member.lastName.toUpperCase()}
                                            />
                                        );
                                    })}
                                </div>
                            </div>
                        )
                    })}
                </div>
            </CardContent>
        </Card>
    </div>
);
