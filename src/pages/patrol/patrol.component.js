import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {Helmet} from 'react-helmet';

// Layout
import LoggedInLayout from '../../layout/loggedIn';

// Components
import Loading from '../../components/loadingOverlay/index';
import PatrolHeader from '../../components/header';
import PatrolDetails from './components/details';
import PatrolTeams from './components/teams';
import PatrolMembers from './components/members';
import PatrolWeather from './components/weather';
import Container from '../../components/container/index';

import Button from '@material-ui/core/Button';

// Services and Helpers
import EventService from '../../services/eventService';

class Patrol extends Component
{
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        EventService.show(this.props.match.params.id);
    }

    getRelevantAction() {
        if(this.props.event.attributes.activeInTeam) {
            return (
                <Button
                    variant="contained"
                    style={{marginTop: '10px', backgroundColor: '#c66600', color: '#ffffff'}}
                >
                    Request Substitute
                </Button>
            );
        }

        if(this.props.event.attributes.activeAsMember) {
            const {leavePatrol} = this.props;
            return (
                <Button
                    variant="contained"
                    style={{marginTop: '10px', backgroundColor: '#9e0000', color: '#ffffff'}}
                    onClick={(e) => {leavePatrol()}}
                >
                    Leave Patrol
                </Button>
            );
        }

        const {joinPatrol} = this.props;
        return (
            <Button
                variant="contained"
                style={{marginTop: '10px', backgroundColor: '#017002', color: '#ffffff'}}
                onClick={(e) => {joinPatrol()}}
            >
                Join Patrol
            </Button>
        );
    }

    render() {
        if(this.props.event.id != this.props.match.params.id) {
            return (
                <LoggedInLayout>
                    <Helmet>
                        <title>Patrol - Loading | Club Portal</title>
                    </Helmet>
                    <Loading />
                </LoggedInLayout>
            );
        }

        return (
            <LoggedInLayout>
                <Helmet>
                    <title>Patrol - {this.props.event.attributes.startDate} | Club Portal</title>
                </Helmet>
                <PatrolHeader
                    title={this.props.event.attributes.name + ' - ' + this.props.event.attributes.startDate}
                    backLink={true}
                    backLinkAction={() => {this.props.history.push('/patrols')}}
                >
                    {this.getRelevantAction()}
                </PatrolHeader>
                <Container>
                    <PatrolDetails {...this.props} />
                    <PatrolTeams {...this.props} />
                    <PatrolMembers {...this.props} />
                </Container>
            </LoggedInLayout>
        );
    }
}

Patrol.propTypes = {
    event: PropTypes.object
}

export default Patrol;
